# RandSPA.jl

RandSPA stands for randomized SPA. 
It is a variant of the [successive projection algorithm (SPA)](https://doi.org/10.1016/S0169-7439(01)00119-8).
It is associated with the paper *Randomized Successive Projection Algorithm*, by Olivier Vu Thanh, Nicolas Nadisic, and Nicolas Gillis, published in GRETSI 2022 ([preprint here](https://www.researchgate.net/publication/363055631_Randomized_Successive_Projection_Algorithm)).
If using this code in your research, please cite this paper.


## Problem

RandSPA solves the following problem: 
given a data matrix  $`X \in \mathbb{R}^{m \times n}`$ and a factorization rank $`r \in \mathbb{N}^*`$, find an index set $`\mathcal{J}`$ of cardinality $`r`$ such that $`X \approx X(:,\mathcal{J}) H`$ for some nonnegative matrix $` H \in \mathbb{R}^{r \times n}_+ `$.


## Getting started

### Option 1: Install package

If you just want to use our methods SPA and RandSPA, you can install the package from the repo [https://gitlab.com/vuthanho/randspa.jl](https://gitlab.com/vuthanho/randspa.jl). The aforementioned repository is a lighter version that does not contain all the heavy packages needed to run the experiments in [option 2](#option-2-clone-package) :
- Start Julia
- Type `]` to enter Pkg mode
- `add https://gitlab.com/vuthanho/randspa.jl`

Then you can type `using RandSPA` and use the functions `spa` and `randspa` in your code.

### Option 2: Clone package

This is necessary only to reproduce our experiments.

Once cloned or downloaded, enter the project folder and start Julia.

- Type `]` to enter Pkg mode
- Type `activate .` 
- Type `instantiate` to install the necessary packages. 
- Type backspace to leave Pkg mode.

The datasets present in this repository come from [this repository](https://gitlab.com/nnadisic/giant.jl).
On a Unix-like system, they can be downloaded all at once by running the script `download_data.sh` while in the folder `./xp/data/`.


## Experiments

To reproduce the experiments from the paper, run:
- `include("xp/xphsu.jl")`for the hyperspectral unmixing experiment
- `include("xp/kapparuns.jl")` for the experiment with varying $`\nu`$


## License

RandSPA.jl is free software, licensed under the [GNU GPL v3](http://www.gnu.org/licenses/gpl.html).
