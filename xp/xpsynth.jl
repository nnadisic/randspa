# Experiments on synthetic data

using RandSPA

using Random
using LinearAlgebra
using DelimitedFiles
using Plots

include("./util.jl")

# Main test function
function runxp(m::Int,
               n::Int,
               r::Int,
               nbtrials::Int,
               noiselevels::Vector)
    # Seed for "reproducible random" (for successive runs only; it may
    # not reproduce on different machines or Julia versions)
    Random.seed!(1234)

    # Results holders
    mrsa_spa = Vector{}(undef, length(noiselevels))
    mrsa_randspa = Matrix{}(undef, nbtrials, length(noiselevels))

    # Run tests once for every noise level
    for (iter, nl) in enumerate(noiselevels)
        # Generate data
        W = rand(m, r)
        H = hcat(I, rand(r, n - r)) # Identity followed by rand columns

        # Normalize
        for col in eachcol(W)
            col ./= sum(col)
        end
        for col in eachcol(H)
            col ./= sum(col)
        end

        # Compute X and add noise
        X = W * H
        noise = randn(m, n)
        X .+= nl * noise / norm(noise) * norm(X)

        # Try to find W with SPA
        spaK = spa(X, r)
        spaW = X[:, spaK]
        mrsa_spa[iter] = meanmrsa(W, spaW)

        bestW = Matrix{}(undef,1,2)
        bestMRSA = Inf
        for t in 1:nbtrials
            randspaK = randspa(X, r)
            curW = X[:, randspaK]
            mrsa_randspa[t, iter] = meanmrsa(W, curW)
        end
    end

    return mrsa_spa, mrsa_randspa
end

noiselevels = logrange(10e-2, 0.5, 20)
mrsa_spa, mrsa_randspa = runxp(10, 50, 5, 30, noiselevels)

# Tmp
p = plot(noiselevels, mrsa_spa, label="SPA",
         legend=:topleft, xlabel="Noise", ylabel="MRSA",
         xaxis=:log10, yaxis=:log10)
plot!(p, noiselevels, [mean(col) for col in eachcol(mrsa_randspa)],
      label="Mean RandSPA")
plot!(p, noiselevels, [minimum(col) for col in eachcol(mrsa_randspa)],
      label="Best RandSPA")
display(p)

# Save a text files
results = hcat(noiselevels,
               mrsa_spa,
               [mean(col) for col in eachcol(mrsa_randspa)],
               [minimum(col) for col in eachcol(mrsa_randspa)])
writedlm("results.txt", results)
