# Experiments of hyperspectral unmixing on real-world datasets

using RandSPA

using Random
using LinearAlgebra
using Statistics
using MAT
using Plots
plotlyjs()
using StatsPlots
using DelimitedFiles
# default(fontfamily="Computer Modern")

include("util.jl")
include("nnls.jl")
include("hyperspectral.jl")

function runxpkappa(name::String,       # name of the dataset, to be used in results files
                  datafile::String,     # name of the .mat file containing the data
                  datavarname::String,  # name of the data matrix in the file
                  transposedata::Bool,  # true if the data matrix is transposed
                  rank::Int,            # factorization rank
                  trials::Int,          # number of runs for averaging
                  runs,                 # list of nb of runs to test kappa
                  kappas,               # list of kappas to test
                  snr=Inf,              # Signal to noise ratio in dB
                  )
    # Seed for "reproducible random" (for successive runs only; it may
    # not reproduce on different machines or Julia versions)
    Random.seed!(1)

    # Announce
    print("$(uppercase(name))")

    # Read data
    data = matopen(datafile)
    X = Array(read(data, datavarname))
    # Transpose data if necessary
    if transposedata
        X = X'
    end
    if snr!=Inf
        N = randn(size(X))
        N.*=10^(-snr/20)*norm(X)/norm(N)
        X.+=N
    end
    normalize!(X,Inf)
    m, n = size(X)
    # Try to find W with SPA
    spaK = spa(X, rank)
    spaW = X[:, spaK]
    spaH = matrixactiveset(spaW, X)
    err_spa = relreconstructionerror(X, spaW, spaH)
    println("SPA : $err_spa")
    # Try to find W with RandSPA
    genP = x -> randOrth!(x,1)
    plotres = Vector{Any}(undef,length(runs))
    res_kappa_runs = zeros(length(kappas),trials,length(runs))
    for (iter_run,n_runs) in enumerate(runs)
        kappa_trials = Inf.*ones(length(kappas),trials)
        for (iter_κ , κ) in enumerate(kappas)
            genP = x -> randOrth!(x,1.1)
            for trial in 1:trials
                for run in 1:n_runs
                    randspaK = randspa(X, rank,Prank=κ,genP=genP)
                    curW = X[:, randspaK]
                    curH = matrixactiveset(curW, X)
                    kappa_trials[iter_κ,trial] = min(100*relreconstructionerror(X, curW, curH),kappa_trials[iter_κ,trial]) 
                    print("\r#runs : $n_runs | kappa : $κ | trial : $trial | run : $run | err : $(kappa_trials[iter_κ,trial])             ")
                end
            end
        end
        res_kappa_runs[:,:,iter_run] = kappa_trials
        plotres[iter_run]=boxplot(repeat(1:length(kappas),outer=length(trials)),kappa_trials[:],linewidth=1,
        fillalpha=0.3,label=false,title="nb of runs = $n_runs")
    end
    #return err_spa, err_randspa
    return res_kappa_runs,plotres
end

# trials = 20
# runs = [1,5,10,20]
# kappas = [1,3,6,10,25,50]
trials = 1
runs = [1]
kappas = [1]
# res_kappa_runs,plotres = runxpkappa("samson", "xp/data/samson.mat", "V", false, 3, trials,runs,kappas)
# xnames = string.(kappas)
# xnames[1] = "VCA"
# for p in plotres
#     xticks!(p,1:length(kappas),xnames)
#     xlabel!(p,"Rank ν of Q")
#     ylabel!(p,"Relative error (%)")
# end
# plot(plotres...)
# for (iter,res) in enumerate(eachslice(res_kappa_runs,dims=3))
#     writedlm("res_nbrun$(runs[iter]).txt",res')
# end

snr = 20
res_kappa_runs,plotres = runxpkappa("samson", "xp/data/samson.mat", "V", false, 3, trials,runs,kappas,snr)
xnames = string.(kappas)
xnames[1] = "VCA"
for p in plotres
    xticks!(p,1:length(kappas),xnames)
    xlabel!(p,"Rank ν of Q")
    ylabel!(p,"Relative error (%)")
end
plot(plotres...)
for (iter,res) in enumerate(eachslice(res_kappa_runs,dims=3))
    writedlm("res_nbrun$(runs[iter])_snr$(snr).txt",res')
end 