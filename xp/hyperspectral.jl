# Utilities for hyperspectral images
# Copied from https://gitlab.com/nnadisic/giant.jl

export displayabundancemap, falsecolors

using Images
using Colors

"""
    displayabundancemap(H, nrows, ncols, filename; bw, nbimgperrow, notebook)

Display the mixing matrix resulting from the unmixing of an hyperspectral image with NMF.
H is the mixing matrix, where every column is a pixel, and every row a material.
nrows and ncols are the number of pixels per row and per column of the image.
filename is the name of the file where to save the resulting image.
bw is an optinal boolean, if true the grayscale is inverted (can produce nicer images).
nbimgperrow is the number of subimages (materials) to display per row of the final image.
notebook must be set to true to display the image when working in a notebook.
"""
function displayabundancemap(trueH, nrows, ncols, filename;
                             bw=true, nbimgperrow=0, notebook=false)
    # Copy matrix to avoid changing the original one
    H = copy(trueH)
    # Get  dimensions and check them
    r, n = size(H)
    nrows*ncols == n || error("The dimensions of the image don't match the parameters")

    # Init array of subimages
    subimages = []

    # Create one subimage per material
    for row in eachrow(H)
        # Normalize so that values are in [0, 1]
        max = maximum(row)
        if max != 0
            row ./= max
        end
        # Create the subimage
        img = reshape(row, nrows, ncols)
        # Add borders (bottom and right)
        img = hcat(img, ones(nrows))
        img = vcat(img, ones(ncols+1)')
        # Revert black and white if needed
        if bw
            img = ones(size(img)) - img
        end
        # Save it
        push!(subimages, img)
    end

    # If number of subimages per row is not defined, set it to r (all in one line)
    if nbimgperrow == 0
        nbimgperrow = r
    end

    # Compute number of lines to display
    nblines, remainder = divrem(r, nbimgperrow)

    # If the number of subimages is not a multiple of nbimgperrow, add white subimages
    if remainder != 0
        nblines += 1
        for i in 1:(nbimgperrow - remainder)
            push!(subimages, ones(nrows+1, ncols+1))
        end
    end

    # Build lines
    lines = []
    for l in 1:nblines
        push!(lines, hcat(subimages[(l-1)*nbimgperrow+1:l*nbimgperrow]...))
    end

    # Concatenate lines
    imgarr = vcat(lines...)

    # Remove last column and last line (useless borders)
    imgarr = imgarr[1:end-1,1:end-1]

    # Save image
    imgview = colorview(Gray, imgarr)
    save(filename, imgview)
    if notebook
        return imgview
    end
end


function falsecolors(X::AbstractMatrix,Xheight,Xwidth,filename)
    # listofcolors = parse.(Colorant,["#ffd700" "#00ff00" "#00ffff" "#ff00ff" "#1e90ff" "#f5deb3" "#ff69b4" "#2f4f4f" "#8b4513" "#228b22" "#00008b" "#ff0000"])
    listofcolors = parse.(Colorant,["#0000ff" "#00ff00" "#ff4500" "#00ffff" "#6495ed" "#008000" "#eee8aa" "#2f4f4f" "#4b0082" "#ff00ff" "#eee8aa" "#ff69b4" "#ffff00"])
    r,n=size(X)
    tmp = copy(X)
    normalize!(tmp,Inf)
    listofcolors = listofcolors[sortperm(sum(tmp,dims=2)[:],rev=true)]
    img = listofcolors[1:r]'*tmp
    img = Matrix(reshape(img,Xheight,Xwidth))
    img = map(clamp01nan, img)
    save(filename,img)
end
