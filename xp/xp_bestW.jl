# Experiments of hyperspectral unmixing on real-world datasets

using RandSPA

using LinearAlgebra
using Statistics
using MAT

include("util.jl")
include("nnls.jl")

function dispres(res)
    println("err_criteria - err_min")
    minerr = minimum(res[:,1])
    println("run with min error     : $(round(minerr,digits=4))")
    println("dif with min volume    : $(round(res[argmin(res[:,2]),1] - minerr,digits=4))")
    println("dif with max volume    : $(round(res[argmax(res[:,2]),1] - minerr,digits=4))")
    println("dif with max norm(X-UU'X) : $(round(res[argmin(res[:,3]),1] - minerr,digits=4))")
    println("average diff with err_min : $(mean(res[1:end.!=argmin(res[:,1]),1].-minerr))")
end

function runxphsu(name::String,        # name of the dataset, to be used in results files
                  datafile::String,    # name of the .mat file containing the data
                  datavarname::String, # name of the data matrix in the file
                  transposedata::Bool, # true if the data matrix is transposed
                  rank::Int,           # factorization rank
                  npr::Int,            # for display, number of pixels per row in the image
                  npc::Int,            # for display, number of pixels per column in the image
                  nir::Int,            # for display, number of images per row of abundance map
                  bw::Bool,        # false to print white on black
                  runs       # number of runs for RandSPA
                  )
    res=zeros(runs,3)
    # Read data
    data = matopen(datafile)
    X = Array(read(data, datavarname))
    # Transpose data if necessary
    if transposedata
        X = X'
    end
    m, n = size(X)

    # Try to find W with RandSPA
    bestW = Matrix{Float64}(undef, m, rank)
    bestH = Matrix{Float64}(undef, rank, n)
    for iter in 1:runs
        randspaK, U = randspa(X, rank,d=1,returnU=true)
        curW = X[:, randspaK]
        curH = matrixactiveset(curW, X)
        res[iter,1] = 100*relreconstructionerror(X, curW, curH)
        res[iter,2] = log(det(curW'curW))
        res[iter,3] = norm(X-U*U'X)
    end

    return res
end
runs = 20
if Sys.iswindows()
    res = runxphsu("jasper", "xp\\data\\jasper.mat", "Y", false, 4, 100, 100, 4, false, runs)
    dispres(res)
    res = runxphsu("samson", "xp\\data\\samson.mat", "V", false, 3, 95, 95, 3, false, runs)
    dispres(res)
else
    res = runxphsu("jasper", "xp/data/jasper.mat", "Y", false, 4, 100, 100, 4, false, runs)
    dispres(res)
    res = runxphsu("samson", "xp/data/samson.mat", "V", false, 3, 95, 95, 3, false, runs)
    dispres(res)
end
