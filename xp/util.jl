# Auxiliary functions

using Statistics
using LinearAlgebra
using Hungarian

# Function similar to Matlab's logspace
logrange(x1, x2, n) = collect(10^y for y in range(log10(x1), log10(x2), length=n))


# Returns a permutation of columns of W such that it is as close as possible to W0
function closestperm(W, W0)
    normW0 = copy(W0)
    for col in eachcol(normW0)
        col ./= norm(col)
    end
    normW = copy(W)
    for col in eachcol(normW)
        col ./= norm(col)
    end
    costmat = -normW0' * normW
    # Safety
    if iszero(length(costmat)) || !isnothing(findfirst(isnan, costmat))
        return zeros(size(W))
    end
    permutation, error = hungarian(costmat)
    filter!(!iszero, permutation)
    return permutation
end


# Compute the cosine of the maximum angle between the columns of W0 and W
function maxangle(W, W0)
    if length(W) < length(W0)
        return 0.0
    end
    permW = closestperm(W, W0)
    normW0 = copy(W0)
    for col in eachcol(normW0)
        col ./= norm(col)
    end
    normW = copy(permW)
    for col in eachcol(normW)
        col ./= norm(col)
    end
    diff = normW0' * normW
    # Safety
    if iszero(length(diff)) || !isnothing(findfirst(isnan, diff))
        return 0.0
    end
    return minimum(diag(diff))
end



# Compute the mean removed spectral angle (MRSA) between two vectors
function mrsa(x, y)
    x = x .- mean(x)
    y = y .- mean(y)
    score = 0
    if (norm(x) > 0) && (norm(y) > 0)
        x = x / norm(x)
        y = y / norm(y)
        val = min(x' * y, 1.0)
        score = abs(acos(val)) * 100 / π
    end
    return score
end

# Compute the maximum MRSA between two sets of vectors
function maxmrsa(W, W0)
    if length(W) < length(W0)
        return 1.0
    end
    permW = closestperm(W, W0)
    maxangle = 0.0
    for j in axes(W0, 2)
        curangle = mrsa(W[:,permW[j]], W0[:,j])
        maxangle = max(maxangle, curangle)
    end
    if isnan(maxangle)
        maxangle = 1.0
    end
    return maxangle
end

# Compute the mean MRSA between two sets of vectors
function meanmrsa(W, W0)
    if length(W) < length(W0)
        return 1.0
    end
    permW = closestperm(W, W0)
    angles = zeros(size(W0, 2))
    for j in axes(W0, 2)
        angles[j] = mrsa(W[:,permW[j]], W0[:,j])
    end
    return mean(angles)
end


function relreconstructionerror(M, W, H)
    return norm(M - W*H) / norm(M)
end
