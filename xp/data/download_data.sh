#!/usr/bin/env bash

wget -nc https://gitlab.com/nnadisic/giant.jl/-/raw/master/xp/data/Cuprite.mat
wget -nc https://gitlab.com/nnadisic/giant.jl/-/raw/master/xp/data/jasper.mat
wget -nc https://gitlab.com/nnadisic/giant.jl/-/raw/master/xp/data/samson.mat
wget -nc https://gitlab.com/nnadisic/giant.jl/-/raw/master/xp/data/SanDiego.mat
wget -nc https://gitlab.com/nnadisic/giant.jl/-/raw/master/xp/data/Urban.mat
