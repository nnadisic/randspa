Put here the datasets downloaded from
https://gitlab.com/nnadisic/giant.jl/-/tree/master/xp/data

From a GNU/Linux shell, you can download them automatically with the following command
bash download_data.sh
