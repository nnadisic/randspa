# Experiments of hyperspectral unmixing on real-world datasets

using RandSPA

using Random
using LinearAlgebra
using Statistics
using MAT
using DelimitedFiles
# using Plots

include("util.jl")
include("nnls.jl")
include("hyperspectral.jl")

function runxphsu(name::String,        # name of the dataset, to be used in results files
                  datafile::String,    # name of the .mat file containing the data
                  datavarname::String, # name of the data matrix in the file
                  transposedata::Bool, # true if the data matrix is transposed
                  rank::Int,           # factorization rank
                  npr::Int,            # for display, number of pixels per row in the image
                  npc::Int,            # for display, number of pixels per column in the image
                  nir::Int,            # for display, number of images per row of abundance map
                  nbtrials::Int        # number of runs for RandSPA
                  )
    # Seed for "reproducible random" (for successive runs only; it may
    # not reproduce on different machines or Julia versions)
    Random.seed!(1)

    # Announce
    print("$(uppercase(name))")

    # Read data
    data = matopen(datafile)
    X = Array(read(data, datavarname))
    # Transpose data if necessary
    if transposedata
        X = X'
    end
    normalize!(X,Inf)
    m, n = size(X)
    print(" m = $m, n = $n")

    # Try to find W with SPA
    spaK = spa(X, rank)
    spaW = X[:, spaK]
    spaH = matrixactiveset(spaW, X)
    err_spa = relreconstructionerror(X, spaW, spaH)

    # Try to find W with RandSPA
    err_randspa = [Inf for _ in 1:nbtrials]
    randspaW = Matrix{Float64}(undef, m, rank)
    randspaH = Matrix{Float64}(undef, rank, n)
    genP = x -> randOrth!(x,1.5)
    for t in 1:nbtrials
        randspaK = randspa(X, rank,Prank=rank+1,genP=genP)
        curW = X[:, randspaK]
        curH = matrixactiveset(curW, X)
        err_randspa[t] = relreconstructionerror(X, curW, curH)
        if argmin(err_randspa) == t
            randspaW .= curW
            randspaH .= curH
        end
    end

    # Try to find W with VCA
    err_vca = [Inf for _ in 1:nbtrials]
    vcaW = Matrix{Float64}(undef, m, rank)
    vcaH = Matrix{Float64}(undef, rank, n)
    for t in 1:nbtrials
        vcaK = randspa(X, rank,Prank=1)
        curW = X[:, vcaK]
        curH = matrixactiveset(curW, X)
        err_vca[t] = relreconstructionerror(X, curW, curH)
        if argmin(err_vca) == t
            vcaW .= curW
            vcaH .= curH
        end
    end

    vcaH = vcaH[closestperm(vcaW,randspaW),:]
    spaH = spaH[closestperm(spaW,randspaW),:]

    # Display abundance maps
    displayabundancemap(spaH, npr, npc, "out-$(name)-spa.png", bw=false, nbimgperrow=nir)
    displayabundancemap(randspaH, npr, npc, "out-$(name)-randspa.png", bw=false, nbimgperrow=nir)
    displayabundancemap(vcaH, npr, npc, "out-$(name)-vca.png", bw=false, nbimgperrow=nir)

    displayabundancemap(spaH, npr, npc, "out-w-$(name)-spa.png", bw=true, nbimgperrow=nir)
    displayabundancemap(randspaH, npr, npc, "out-w-$(name)-randspa.png", bw=true, nbimgperrow=nir)
    displayabundancemap(vcaH, npr, npc, "out-w-$(name)-vca.png", bw=true, nbimgperrow=nir)

    # Display false color maps
    falsecolors(spaH,npr,npc,"false-$(name)-spa.png")
    falsecolors(randspaH,npr,npc,"false-$(name)-randspa.png")
    falsecolors(vcaH,npr,npc,"false-$(name)-vca.png")

    # endmembers = plot(spaW,linecolor = :red,label=hcat("SPA",[false for i in 1:rank-1]'))
    # plot!(endmembers,bestW,linecolor = :black,label=hcat("RandSPA",[false for i in 1:rank-1]'))
    # display(endmembers)
    println("\t$(round(100*err_spa, digits=4))" *
        "\t$(round(100*median(err_randspa), digits=4))" *
        "\t\t$(round(100*minimum(err_randspa), digits=4))" *
        "\t$(round(100*median(err_vca), digits=4))" * 
        "\t\t$(round(100*minimum(err_vca), digits=4))")

    return err_spa, err_randspa, err_vca
    # return nothing
end

function zaeiurh(err_spa, err_randspa, err_vca)
    return [round(100*err_spa, digits=4) round(100*median(err_randspa), digits=4) round(100*minimum(err_randspa), digits=4) round(100*median(err_vca), digits=4) round(100*minimum(err_vca), digits=4)]
end

println("Dataset\t\t\t\tSPA\tMedian RandSPA\tBest RandSPA\tMedian VCA\tBest VCA")
err_spa, err_randspa, err_vca = runxphsu("jasper", "xp/data/jasper.mat", "Y", false, 4, 100, 100, 4, 30)
A=zaeiurh(err_spa, err_randspa, err_vca)
err_spa, err_randspa, err_vca = runxphsu("samson", "xp/data/samson.mat", "V", false, 3, 95, 95, 3, 30)
A=[A;zaeiurh(err_spa, err_randspa, err_vca)]
err_spa, err_randspa, err_vca = runxphsu("urban", "xp/data/Urban.mat", "A", true, 5, 307, 307, 5, 30)
A=[A;zaeiurh(err_spa, err_randspa, err_vca)]
err_spa, err_randspa, err_vca = runxphsu("cuprite", "xp/data/Cuprite.mat", "x", false, 12, 250, 191, 4, 30)
A=[A;zaeiurh(err_spa, err_randspa, err_vca)]
err_spa, err_randspa, err_vca = runxphsu("sandieg", "xp/data/SanDiego.mat", "A", true, 8, 400, 400, 4, 30)
A=[A;zaeiurh(err_spa, err_randspa, err_vca)]

writedlm("res_xphsu.txt", A)