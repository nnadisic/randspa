using RandSPA
using LinearAlgebra

# Parameters
m = 5
n = 10
r = 3

# Generate data
realW = rand(m, r)
realH = hcat(I, rand(r, n-r)) # Identity followed by rand columns

# Normalize
for col in eachcol(realW)
    col ./= sum(col)
end
for col in eachcol(realH)
    col ./= sum(col)
end

# Compute X
X = realW * realH

# Try to find W with SPA
println("Running SPA")
@time spaK = spa(X, r)
display(spaK)

println("Running randSPA")
@time randspaK = randspa(X, r)
display(randspaK)

g = x->randOrth!(x,Inf)
println("Running randSPA like VCA")
randspaK = randspa(rand(2,2), 2, genP=g)
@time randspaK = randspa(X, r, genP=g)
display(randspaK)

g = x->randOrth!(x,1)
println("Running randSPA like SPA")
randspaK = randspa(rand(2,2), 2, genP=g)
@time randspaK = randspa(X, r, genP=g)
display(randspaK)